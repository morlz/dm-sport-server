<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doc extends Model {
    protected $fillable = [
        'id', 'title', 'size', 'file', 'mime', 'created_at', 'updated_at', 'user_id', 'name'
    ];

	protected $dates = [
		'created_at',
		'updated_at'
	];
	protected $hidden = ['file'];
}
