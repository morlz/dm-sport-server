<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anounce extends Model {
    protected $fillable = [
        'id', 'title', 'image', 'date'
    ];
}
