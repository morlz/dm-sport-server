<?php

namespace App\Http\Controllers;

use App\User;
use \Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        //
    }

	public function signin (Request $request) {
		$res = User::where('login', $request->input('login'))
			->where('pass', $this->hashPass($request->input('pass')))
			->firstOrFail();

		$res->api_token = $this->hashPass(microtime());
		$res->save();
		$res->makeVisible(["api_token"]);

		return response()->json($res, 200);
	}

	public function getUserData (Request $request) {
		return response()->json(Auth::user(), 200);
	}

	private function hashPass ($pass) {
		return hash('sha256', $pass);
	}
}
