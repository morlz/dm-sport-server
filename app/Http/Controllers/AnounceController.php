<?php

namespace App\Http\Controllers;

use App\Anounce;
use \Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AnounceController extends Controller
{
    public function __construct()
    {
        //
    }

	public function getList (Request $request) {
		Anounce::where('date', '<', date('Y-M-D'))->delete();
		$res = Anounce::orderBy('date', 'DESC')
			->get();


		return response()->json($res, 200);
	}

	public function create (Request $request) {
		$model = Anounce::create(array_merge($request->all(), ['user_id' => Auth::user()->id]));
		$model->save();
		return response()->json($model, 200);
	}

	public function update (Request $request, $id) {
		$model = Anounce::findOrFail($id);
		$model->update($request->all());
		return response()->json($model, 200);
	}

	public function delete (Request $request, $id) {
		$model = Anounce::findOrFail($id);
		$model->delete();
		return response()->json($model, 200);
	}
}
