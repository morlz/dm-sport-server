<?php
namespace app\Http\Controllers;

trait VideoTrait {
	public static function normalizeVideoUrl ($url) {
		if (is_null($url))
			return '';

		return str_replace(
			'https://www.youtube.com/watch?v=',
			'https://www.youtube.com/embed/',
			$url
		);
	}
}
