<?php

namespace App\Http\Controllers;

use App\Video;
use \Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VideoController extends Controller
{
	use VideoTrait;

    public function __construct()
    {
        //
    }

	public function getList (Request $request) {
		$res = Video::limit($request->input('limit'))
			->offset($request->input('offset'))
			->orderBy('created_at', 'DESC')
			->get();

		return response()->json($res, 200);
	}

	public function getOne (Request $request, $id) {
		return response()->json(Video::findOrFail($id), 200);
	}

	public function create (Request $request) {
		$model = Video::create(
			array_merge(
				$request->all(),
				[
					'user_id' => Auth::user()->id,
					'url' => self::normalizeVideoUrl($request->input('url'))
				]
			)
		);
		$model->save();
		return response()->json($model, 200);
	}

	public function update (Request $request, $id) {
		$model = Video::findOrFail($id);
		$model->update(array_merge(
			$request->all(),
			[
				'url' => self::normalizeVideoUrl($request->input('url'))
			]
		));
		return response()->json($model, 200);
	}

	public function delete (Request $request, $id) {
		$model = Video::findOrFail($id);
		$model->delete();
		return response()->json($model, 200);
	}
}
