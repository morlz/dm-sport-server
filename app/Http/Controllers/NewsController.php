<?php

namespace App\Http\Controllers;

use App\News;
use \Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NewsController extends Controller
{
	use VideoTrait;

    public function __construct()
    {
        //
    }

	public function getList (Request $request) {
		$res = News::where('type', $request->input('type'))
			->limit($request->input('limit'))
			->offset($request->input('offset'))
			->orderBy('created_at', 'DESC')
			->get();

		return response()->json($res, 200);
	}

	public function getOne (Request $request, $id) {
		return response()->json(News::findOrFail($id), 200);
	}

	public function create (Request $request) {
		$model = News::create(
			array_merge(
				$request->all(),
				[
					'user_id' => Auth::user()->id,
					'video' => self::normalizeVideoUrl($request->input('video'))
				]
			)
		);
		$model->save();
		return response()->json($model, 200);
	}

	public function update (Request $request, $id) {
		$model = News::findOrFail($id);
		$model->update(
			array_merge(
				$request->all(),
				[
					'video' => self::normalizeVideoUrl($request->input('video'))
				]
			)
		);
		return response()->json($model, 200);
	}

	public function delete (Request $request, $id) {
		$model = News::findOrFail($id);
		$model->delete();
		return response()->json($model, 200);
	}
}
