<?php

namespace App\Http\Controllers;

use App\Doc;
use \Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DocController extends Controller
{

    public function __construct()
    {
        //
    }

	public function getList (Request $request) {
		$res = Doc::limit($request->input('limit'))
			->select([
				'id', 'title', 'size', 'mime', 'created_at', 'updated_at', 'name'
			])
			->offset($request->input('offset'))
			->orderBy('created_at', 'DESC')
			->get();

		return response()->json($res, 200);
	}

	public function getFile (Request $request, $id) {
		$doc = Doc::findOrFail($id);

		return response($doc->file, 200)
			->header('Content-type', $doc->mime)
			->header('Content-Transfer-Encoding', 'binary');
	}

	public function getOne (Request $request, $id) {
		return response()->json(Doc::findOrFail($id), 200);
	}

	public function create (Request $request) {
		if (is_null($request->file('file')))
			abort(400, 'File is null');

		$file = $request->file('file')->openFile('r');

		$model = Doc::create(
			array_merge(
				$request->all(),
				[
					'user_id' => Auth::user()->id,
					//'file' => base64_decode($base64)
					'file' => $file->fread($file->getSize())
				]
			)
		);
		$model->save();
		return response()->json($model->makeHidden('file'), 200);
	}

	public function delete (Request $request, $id) {
		$model = Doc::findOrFail($id);
		$model->delete();
		return response()->json($model, 200);
	}
}
