<?php

namespace App\Http\Controllers;

use App\Page;
use \Illuminate\Http\Request;

class PagesController extends Controller
{
    public function __construct()
    {
        //
    }

	public function getOne (Request $request, $id) {
		return response()->json(Page::findOrFail($id), 200);
	}

	public function update (Request $request, $id) {
		$model = Page::findOrFail($id);
		$model->update($request->all());
		return response()->json($model, 200);
	}
}
