<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model {
    protected $fillable = [
        'id', 'title', 'content', 'video', 'user_id', 'type', 'image', 'created_at', 'updated_at'
    ];

	protected $dates = [
		'created_at', 'updated_at', 'deleted_at'
	];

	public function user () {
		return $this->hasOne('App\User');
	}
}
