<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model {
    protected $fillable = [
        'id', 'title', 'url', 'user_id'
    ];

	protected $dates = [];
}
