<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return 'The API';//$router->app->version();
});

$router->get('news', 'NewsController@getList');
$router->get('new/{id}', 'NewsController@getOne');

$router->get('page/{id}', 'PagesController@getOne');

$router->get('videos', 'VideoController@getList');
$router->get('video/{id}', 'VideoController@getOne');

$router->get('docs', 'DocController@getList');
$router->get('doc/{id}', 'DocController@getOne');
$router->get('file/{id}', 'DocController@getFile');

$router->get('anounces', 'AnounceController@getList');

$router->post('signin', 'UserController@signin');


$router->group(['middleware' => ['auth']], function () use ($router) {
	$router->get('userdata', 'UserController@getUserData');

	$router->post('new', 'NewsController@create');
	$router->put('new/{id}', 'NewsController@update');
	$router->delete('new/{id}', 'NewsController@delete');

	$router->put('page/{id}', 'PagesController@update');

	$router->post('video', 'VideoController@create');
	$router->put('video/{id}', 'VideoController@update');
	$router->delete('video/{id}', 'VideoController@delete');

	$router->post('doc', 'DocController@create');
	$router->delete('doc/{id}', 'DocController@delete');

	$router->post('anounce', 'AnounceController@create');
	$router->put('anounce/{id}', 'AnounceController@update');
	$router->delete('anounce/{id}', 'AnounceController@delete');
});
